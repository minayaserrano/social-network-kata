# Social Network Kata

We need to develop a Social Network.

Requirements - Console API:

1. Posting: Alice can publish messages to a personal timeline

2. Reading: Bob can view Alice’s timeline

3. Following: Charlie can subscribe to Alice’s and Bob’s timelines, and view an aggregated list of all subscriptions

4. Mentions: Bob can link to Charlie in a message using “@”

5. Links: Alice can link to a clickable web resource in a message

6. Direct Messages: Mallory can send a private message to Alice

References:

http://kata-log.rocks/social-network-kata

http://monospacedmonologues.com/post/49250842364/the-social-networking-kata

Tests execution:

```
python -m unittest discover -s tests -v
```